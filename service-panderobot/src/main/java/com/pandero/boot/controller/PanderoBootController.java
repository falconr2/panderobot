package com.pandero.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pandero.boot.request.PanderoBot;
import com.pandero.boot.service.PanderoBootService;

@Controller
public class PanderoBootController {
	
	@Autowired
	PanderoBootService panderoBootService;
	
	@RequestMapping(value = "/registrar/panderoBot", method = RequestMethod.POST)
	public ResponseEntity<?> recibirDatosPanderoBot(@RequestBody PanderoBot request) throws Exception {
		return new ResponseEntity<>(
				panderoBootService.registrarDatosAsociado(request),
				HttpStatus.OK);
	}

}
