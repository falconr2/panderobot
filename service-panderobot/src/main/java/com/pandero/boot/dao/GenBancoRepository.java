package com.pandero.boot.dao;

import org.springframework.data.repository.CrudRepository;

import com.pandero.boot.entity.GenBanco;

public interface GenBancoRepository extends CrudRepository<GenBanco, Long> {
	public GenBanco findByCodigoBancoASBANC(String codigoBancoASBANC);
}
