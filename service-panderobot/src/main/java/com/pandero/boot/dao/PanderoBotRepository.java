package com.pandero.boot.dao;

import org.springframework.data.repository.CrudRepository;

import com.pandero.boot.entity.PerPanderoBot;

public interface PanderoBotRepository extends CrudRepository<PerPanderoBot, Long>{

}
