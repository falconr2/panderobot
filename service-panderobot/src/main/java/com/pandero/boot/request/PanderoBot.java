package com.pandero.boot.request;

public class PanderoBot {

	private String personaNombre;
	private String personaCorreo;
	private String personaTelefono;
	private String producto;

	public String getPersonaNombre() {
		return personaNombre;
	}

	public void setPersonaNombre(String personaNombre) {
		this.personaNombre = personaNombre;
	}

	public String getPersonaCorreo() {
		return personaCorreo;
	}

	public void setPersonaCorreo(String personaCorreo) {
		this.personaCorreo = personaCorreo;
	}

	public String getPersonaTelefono() {
		return personaTelefono;
	}

	public void setPersonaTelefono(String personaTelefono) {
		this.personaTelefono = personaTelefono;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

}
