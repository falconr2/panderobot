package com.pandero.boot;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.google.common.base.Preconditions;

@Configuration 
@PropertySource({ "classpath:persistence-db.properties" })
@EnableJpaRepositories(basePackages= "com.pandero.boot.dao", 
						entityManagerFactoryRef = "sqlserverEntityManager", 
						transactionManagerRef = "sqlserverTransactionManager")
public class DataSourceSqlServerConfig {
    @Autowired
    private Environment env;

    public DataSourceSqlServerConfig() {
        super();
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean sqlserverEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(userDataSource());
        em.setPackagesToScan(new String[] { "com.pandero.boot.entity" });

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        final HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.show_sql", env.getProperty("sqlserver.hibernate.show_sql"));
        properties.put("hibernate.dialect", env.getProperty("sqlserver.hibernate.dialect"));
        em.setJpaPropertyMap(properties);

        return em;
    }
    
    @Primary
    @Bean
    public DataSource userDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Preconditions.checkNotNull(env.getProperty("sqlserver.jdbc.driverClassName")));
        dataSource.setUrl(Preconditions.checkNotNull(env.getProperty("sqlserver.jdbc.url")));
        dataSource.setUsername(Preconditions.checkNotNull(env.getProperty("sqlserver.jdbc.user")));
        dataSource.setPassword(Preconditions.checkNotNull(env.getProperty("sqlserver.jdbc.pass")));
        return dataSource;
    }

    @Primary
    @Bean
    public PlatformTransactionManager sqlserverTransactionManager() {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(sqlserverEntityManager().getObject());
        return transactionManager;
    }
}
