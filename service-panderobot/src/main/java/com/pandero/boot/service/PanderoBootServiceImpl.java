package com.pandero.boot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pandero.boot.dao.PanderoBotRepository;
import com.pandero.boot.entity.PerPanderoBot;
import com.pandero.boot.request.PanderoBot;

@Service
public class PanderoBootServiceImpl implements PanderoBootService{
	
	@Autowired
	PanderoBotRepository panderoBotRepository;

	@Override
	public String registrarDatosAsociado(PanderoBot request) {
		
		
		PerPanderoBot panderoBot = new PerPanderoBot();
		panderoBot.setNombre(request.getPersonaNombre());
		panderoBot.setCorreo(request.getPersonaCorreo());
		panderoBot.setTelefono(request.getPersonaTelefono());
		panderoBot.setProducto(request.getProducto());
		
		panderoBotRepository.save(panderoBot);
		
		
		
		return "Nos comunicaremos con usted " + request.getPersonaNombre();
	}

}
