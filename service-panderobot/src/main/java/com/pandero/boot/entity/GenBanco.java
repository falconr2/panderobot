package com.pandero.boot.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GEN_BANCO")
public class GenBanco implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BancoId")
	private Long bancoId;
	@Column(name="BancoNombre")
	private String bancoNombre;
	@Column(name="BancoNombreCorto")
	private String bancoNombreCorto;
	@Column(name="BancoCodigoCONASEV")
	private String bancoCodigoCONASEV;
	@Column(name="BancoDeposito")
	private Boolean bancoDeposito;
	@Column(name="BancoExtranjero")
	private Boolean bancoExtranjero;
	@Column(name="BancoActivo")
	private Boolean bancoActivo;
	@Column(name="BancoProcesoCobranza")
	private Boolean bancoProcesoCobranza;
	@Column(name="BancoCuentaEmpresa")
	private String bancoCuentaEmpresa;
	@Column(name="CodigoBancoASBANC")
	private String codigoBancoASBANC;
	@Column(name="BancoCuentaContable")
	private String bancoCuentaContable;
	@Column(name="BancoNombreVisa")
	private String bancoNombreVisa;
	
	public Long getBancoId() {
		return bancoId;
	}
	public void setBancoId(Long bancoId) {
		this.bancoId = bancoId;
	}
	public String getBancoNombre() {
		return bancoNombre;
	}
	public void setBancoNombre(String bancoNombre) {
		this.bancoNombre = bancoNombre;
	}
	public String getBancoNombreCorto() {
		return bancoNombreCorto;
	}
	public void setBancoNombreCorto(String bancoNombreCorto) {
		this.bancoNombreCorto = bancoNombreCorto;
	}
	public String getBancoCodigoCONASEV() {
		return bancoCodigoCONASEV;
	}
	public void setBancoCodigoCONASEV(String bancoCodigoCONASEV) {
		this.bancoCodigoCONASEV = bancoCodigoCONASEV;
	}
	public Boolean getBancoDeposito() {
		return bancoDeposito;
	}
	public void setBancoDeposito(Boolean bancoDeposito) {
		this.bancoDeposito = bancoDeposito;
	}
	public Boolean getBancoExtranjero() {
		return bancoExtranjero;
	}
	public void setBancoExtranjero(Boolean bancoExtranjero) {
		this.bancoExtranjero = bancoExtranjero;
	}
	public Boolean getBancoActivo() {
		return bancoActivo;
	}
	public void setBancoActivo(Boolean bancoActivo) {
		this.bancoActivo = bancoActivo;
	}
	public String getBancoCuentaContable() {
		return bancoCuentaContable;
	}
	public void setBancoCuentaContable(String bancoCuentaContable) {
		this.bancoCuentaContable = bancoCuentaContable;
	}
	public String getBancoCuentaEmpresa() {
		return bancoCuentaEmpresa;
	}
	public void setBancoCuentaEmpresa(String bancoCuentaEmpresa) {
		this.bancoCuentaEmpresa = bancoCuentaEmpresa;
	}
	public String getCodigoBancoASBANC() {
		return codigoBancoASBANC;
	}
	public void setCodigoBancoASBANC(String codigoBancoASBANC) {
		this.codigoBancoASBANC = codigoBancoASBANC;
	}
	public String getBancoNombreVisa() {
		return bancoNombreVisa;
	}
	public void setBancoNombreVisa(String bancoNombreVisa) {
		this.bancoNombreVisa = bancoNombreVisa;
	}
	public Boolean getBancoProcesoCobranza() {
		return bancoProcesoCobranza;
	}
	public void setBancoProcesoCobranza(Boolean bancoProcesoCobranza) {
		this.bancoProcesoCobranza = bancoProcesoCobranza;
	}
	

}
